package com.shruti.newjob.fabricmanager;

import android.util.Log;

import com.shruti.newjob.user.FeedUser;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.tweetui.TimelineResult;
import com.twitter.sdk.android.tweetui.UserTimeline;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Enum that uses Fabric Android SDK
 * to fetch tweets
 */
public enum FabricManager {

    INSTANCE;
    private ArrayList<FeedUser> feedUsers = new ArrayList<>();

    FabricManager() {
    }


    /**
     * fetches the tweets on HumbleBrag's timeline.
     * @param taskCompleted, callback which returns the list of tweets
     */
    public void getTweets(final OnTaskCompleted taskCompleted) {
        UserTimeline userTimeline = new UserTimeline.Builder().maxItemsPerRequest(100).screenName("HumbleBrag").build();
        userTimeline.next(null, new Callback<TimelineResult<Tweet>>() {
            @Override
            public void success(Result<TimelineResult<Tweet>> result) {
                Log.d("MYMESSAGE", "No of Tweets :: " + result.data.items.size());
                List<Tweet> tweets = result.data.items;
                taskCompleted.success(populateList(tweets));
            }

            @Override
            public void failure(TwitterException e) {
                Log.d("MYMESSAGE", "failed...");
                taskCompleted.failure(e);
            }
        });
    }

    /**
     * accepts list of Tweet objects and coverts it to
     * list of FeedUser objects
     * @param tweets list of Tweet objects
     * @return ArrayList of FeedUser objects
     */
    private ArrayList<FeedUser> populateList(List<Tweet> tweets) {
        for (Tweet tweet : tweets) {
            User user;
            if (tweet.retweetedStatus == null) {
                user = tweet.user;
            } else {
                user = tweet.retweetedStatus.user;
            }
            String tweetDate = formatDate(tweet.createdAt);
            feedUsers.add(new FeedUser(user.screenName, user.name, user.profileImageUrlHttps, user.profileBannerUrl, tweet.text, tweetDate, tweet.retweetCount, tweet.favoriteCount));
        }
        return feedUsers;
    }

    /**
     * Callback Interface
     */
    public interface OnTaskCompleted {
        void success(ArrayList<FeedUser> result);
        void failure(TwitterException e);
    }

    /**
     * Formats Tweet Date
     * @param date Tweet date fetched from API in form of 'EEE MMM d HH:mm:ss Z yyyy'
     * @return formatted date in form of 'MMM d, yyyy'
     */
    private String formatDate(String date) {
        String formattedDate = "";
        try {

            SimpleDateFormat sourceDateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy");
            Date newdate = sourceDateFormat.parse(date);
            SimpleDateFormat targetDateFormat = new SimpleDateFormat("MMM d, yyyy");
            formattedDate = targetDateFormat.format(newdate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }


    /**
     *
     * @return list of FeedUser objects
     */
    public List<FeedUser> getFeedUsers() {
        return feedUsers;
    }

}