package com.shruti.newjob.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.shruti.newjob.constants.GlobalApp;
import com.shruti.newjob.R;
import com.shruti.newjob.constants.TwitterConstants;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import io.fabric.sdk.android.Fabric;


public class MainActivity extends AppCompatActivity {
    TwitterLoginButton loginButton;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Authenticating Twitter Key and Twitter Secret using Fabric
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TwitterConstants.TWITTER_KEY, TwitterConstants.TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_main);

        overridePendingTransition(R.anim.open_scale, R.anim.close_translate);

        tv = (TextView) findViewById(R.id.appName);
        Animation appNameAnim = AnimationUtils.loadAnimation(this, R.anim.appnameanim);

        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                GlobalApp.twitterSession = result.data;
                userLoggedin();
            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
                Snackbar.make(tv, R.string.loginfail, Snackbar.LENGTH_LONG).show();
            }
        });
        tv.startAnimation(appNameAnim);

        appNameAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d("TwitterKit", checkLoginStatus() + "status");

                //if the user is not logged in, login button is made visible.

                if (checkLoginStatus()) {
                    userLoggedin();
                } else {
                    loginButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });


    }

    /**
     * checks if a user has already logged in or is using the app for the first time.
     * @return true, if already logged in, false otherwise.
     */

    boolean checkLoginStatus() {
        if (Twitter.getInstance().core.getSessionManager().getActiveSession() == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.open_translate, R.anim.close_translate);
    }

    /**
     * saves the logged in user's session in GlobalApp
     * and finishes the activity
     */
    void userLoggedin() {
        loginButton.setVisibility(View.INVISIBLE);
        GlobalApp.twitterSession = Twitter.getInstance().core.getSessionManager().getActiveSession();
        String msg = "@" + GlobalApp.twitterSession.getUserName() + " logged in! (#" + GlobalApp.twitterSession.getUserId() + ")";
        Log.d("TwitterKit", msg);
        finish();
        Intent intent = new Intent(this, TweetsActivity.class);
        startActivity(intent);//, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Pass the activity result to the login button.
        loginButton.onActivityResult(requestCode, resultCode, data);
    }
}
