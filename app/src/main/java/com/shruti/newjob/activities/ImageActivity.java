package com.shruti.newjob.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.shruti.newjob.R;
import com.shruti.newjob.adapter.HackyViewPager;
import com.shruti.newjob.adapter.ImageAdapter;
import com.shruti.newjob.fabricmanager.FabricManager;

public class ImageActivity extends AppCompatActivity {

    HackyViewPager mViewPager;
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        int position = getIntent().getIntExtra("position", 0);

        mToolbar.setTitle(FabricManager.INSTANCE.getFeedUsers().get(position).getFullName());
        mToolbar.setSubtitle("@" + FabricManager.INSTANCE.getFeedUsers().get(position).getScreenName());

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
        mViewPager.setAdapter(new ImageAdapter());
        mViewPager.setCurrentItem(position);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                int currentItem = mViewPager.getCurrentItem();
                mToolbar.setTitle(FabricManager.INSTANCE.getFeedUsers().get(currentItem).getFullName());
                mToolbar.setSubtitle("@" + FabricManager.INSTANCE.getFeedUsers().get(currentItem).getScreenName());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        
        overridePendingTransition(R.anim.open_translate, R.anim.close_translate);
    }
}