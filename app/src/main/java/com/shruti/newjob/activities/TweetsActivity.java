package com.shruti.newjob.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.shruti.newjob.adapter.TweetsAdapter;
import com.shruti.newjob.constants.GlobalApp;
import com.shruti.newjob.R;
import com.shruti.newjob.fabricmanager.FabricManager;
import com.shruti.newjob.user.FeedUser;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.TwitterException;

import java.util.ArrayList;

public class TweetsActivity extends AppCompatActivity {

    ProgressBar pb;
    ArrayList<FeedUser> feedUsers = new ArrayList<>();
    RecyclerView tweetRecyclerView;
    TweetsAdapter tweetsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweets);

        Snackbar.make(findViewById(R.id.my_recycler_view), "Logged in as " + GlobalApp.twitterSession.getUserName(), Snackbar.LENGTH_LONG).show();
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.humblebrag);
        mToolbar.setSubtitle("@humblebrag");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pb = (ProgressBar) findViewById(R.id.progressBar);
        tweetRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        tweetRecyclerView.setHasFixedSize(false);
        tweetRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        FabricManager.INSTANCE.getTweets(new FabricManager.OnTaskCompleted() {
            @Override
            public void success(ArrayList<FeedUser> result) {
                feedUsers = result;
                pb.setVisibility(View.INVISIBLE);
                tweetsAdapter = new TweetsAdapter(feedUsers);
                tweetRecyclerView.setAdapter(tweetsAdapter);
            }

            @Override
            public void failure(TwitterException e) {
                Snackbar.make(findViewById(R.id.my_recycler_view), R.string.error_msg, Snackbar.LENGTH_LONG).show();
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        
        overridePendingTransition(R.anim.open_translate, R.anim.close_translate);
    }

}
