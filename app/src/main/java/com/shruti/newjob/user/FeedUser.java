package com.shruti.newjob.user;

import android.util.Log;

/**
 * Used to store tweets data needed to display
 */
public class FeedUser {

    private String screenName;
    private String fullName;
    private String profilePicURL;
    private String bannerURL;
    private String tweetText;
    private String largeProfilePicURL;
    private String tweetDate;
    private int retweetCount;
    private int favCount;

    public FeedUser() {

    }

    public FeedUser(String screenName, String fullName, String profilePicURL, String bannerURL, String tweetText, String tweetDate, int retweetCount, int favCount) {
        this.screenName = screenName;
        this.fullName = fullName;
        this.profilePicURL = profilePicURL;
        this.bannerURL = bannerURL;
        this.tweetText = tweetText;
        this.tweetDate = tweetDate;
        this.retweetCount = retweetCount;
        this.favCount = favCount;
    }

    public String getBannerURL() {
        return bannerURL;
    }

    public String getFullName() {
        return fullName;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public String getTweetText() {
        return tweetText;
    }

    public String getTweetDate() {
        return tweetDate;
    }

    public int getFavCount() {
        return favCount;
    }

    public int getRetweetCount() {
        return retweetCount;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setBannerURL(String bannerURL) {
        this.bannerURL = bannerURL;
    }

    public void setProfilePicURL(String profilePicURL) {
        this.profilePicURL = profilePicURL;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public void setTweetText(String tweetText) {
        this.tweetText = tweetText;
    }

    public void setTweetDate(String tweetDate) {
        this.tweetDate = tweetDate;
    }

    public void setFavCount(int favCount) {
        this.favCount = favCount;
    }

    public void setRetweetCount(int retweetCount) {
        this.retweetCount = retweetCount;
    }

    public boolean isBannerImageAvailable() {
        if (getBannerURL() == null)
            return false;
        else
            return true;
    }

    public String getOriginalProfilePicURL() {
        Log.d("MYMESSAGE", getProfilePicURL());
        if (getProfilePicURL().contains("_normal"))
            largeProfilePicURL = getProfilePicURL().replace("_normal", "");
        return largeProfilePicURL;
    }

    public String getBigProfilePicURL() {
        Log.d("MYMESSAGE", getProfilePicURL());
        if (getProfilePicURL().contains("_normal"))
            largeProfilePicURL = getProfilePicURL().replace("_normal", "_bigger");
        return largeProfilePicURL;
    }
}
