package com.shruti.newjob.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shruti.newjob.R;
import com.shruti.newjob.activities.ImageActivity;
import com.shruti.newjob.user.FeedUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Adapter that adds each tweet view in recycler view
 */
public class TweetsAdapter extends RecyclerView.Adapter<TweetsAdapter.ViewHolder> {

    ArrayList<FeedUser> feedUsers;

    public TweetsAdapter(ArrayList<FeedUser> feedUsers){
        this.feedUsers = feedUsers;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nametv, tweettv, screennametv, tweetDate, retweetCounttv, favCounttv;
        public ImageView bannerimv, userimv;

        public ViewHolder(View v) {
            super(v);
            nametv = (TextView) v.findViewById(R.id.username);
            tweettv = (TextView) v.findViewById(R.id.tweet);
            tweetDate = (TextView) v.findViewById(R.id.tweetDate);
            screennametv = (TextView) v.findViewById(R.id.screenname);
            retweetCounttv = (TextView) v.findViewById(R.id.retweetCount);
            favCounttv = (TextView) v.findViewById(R.id.favCount);
            bannerimv = (ImageView) v.findViewById(R.id.bannerPhoto);
            userimv = (ImageView) v.findViewById(R.id.userPhoto);
            userimv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (Integer) userimv.getTag();
                    Intent intent = new Intent(userimv.getContext(), ImageActivity.class);
                    intent.putExtra("position", position);
                    userimv.getContext().startActivity(intent);
                }
            });
        }
    }

    @Override
    public TweetsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tweetlayout, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        FeedUser feedUser = feedUsers.get(position);

        holder.nametv.setText(feedUser.getFullName());
        holder.screennametv.setText("@" + feedUser.getScreenName());
        holder.tweettv.setText(feedUser.getTweetText());
        holder.tweetDate.setText(feedUser.getTweetDate());
        holder.favCounttv.setText(feedUser.getFavCount() + "");
        holder.retweetCounttv.setText(feedUser.getRetweetCount() + "");
        holder.userimv.setTag(position);
        Picasso.with(holder.userimv.getContext()).load(feedUser.getBigProfilePicURL()).error(android.R.drawable.presence_offline).into(holder.userimv);

        if (feedUser.isBannerImageAvailable()) {
            Uri bannerURL = Uri.parse(feedUser.getBannerURL());
            Picasso.with(holder.bannerimv.getContext()).load(bannerURL).error(android.R.drawable.presence_offline).into(holder.bannerimv);
        } else {
            Picasso.with(holder.bannerimv.getContext()).load(R.drawable.nobanner).into(holder.bannerimv);
        }
    }

    @Override
    public int getItemCount() {
        return feedUsers.size();
    }
}
