package com.shruti.newjob.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.shruti.newjob.R;
import com.shruti.newjob.fabricmanager.FabricManager;
import com.squareup.picasso.Picasso;

import uk.co.senab.photoview.PhotoView;

/**
 * PagerAdapter that instantiates each image
 */
public class ImageAdapter extends PagerAdapter {
    @Override
    public int getCount() {
        return FabricManager.INSTANCE.getFeedUsers().size();
    }

    @Override
    public View instantiateItem(ViewGroup container, int position) {

        PhotoView photoView = new PhotoView(container.getContext());
        Picasso.with(photoView.getContext())
                .load(FabricManager.INSTANCE.getFeedUsers().get(position).getOriginalProfilePicURL())
                .placeholder(R.drawable.animate_progress)
                .error(android.R.drawable.presence_offline)
                .into(photoView);
        container.addView(photoView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        return photoView;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}